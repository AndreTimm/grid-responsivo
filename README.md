# GRID RESPONSIVO #
Grid com 4 colunas responsivas e adaptadas para utiliza��o mobile.
## Usando ##
Adicione dentro da tag head do seu HTML link com refer�ncia ao css.
```html
    <link rel="stylesheet" href="css/grid.css">
```
Para utilizar, basta colocar uma div com a classe row e dentro dela outra div com a classe col mais a coloca que deseja:

```html
   <div class="row">
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
	</div>
	
	<div class="row">
		<div class="col col-2">2</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
	</div>

	<div class="row">
		<div class="col col-2">2</div>
		<div class="col col-2">2</div>
	</div>

	<div class="row">
		<div class="col col-3">3</div>
		<div class="col col-1">2</div>
	</div>

	<div class="row">
		<div class="col col-4">4</div>
	</div>
```
## C�lculos Efetuados ##
### Para achar a margin ###

 	mr = mp / mc

#### Onde: ####
  	mr = margem em porcentagem
 	mp = margem em pixel
    mc = m�ximo de colunas
    
Acahando a margen vamos calcular o width de cada coluna

	scw = (100 � (m * (mc � 1))) / mc
	
	cw = (scw * cs) + (m * (cs � 1))

#### Onde : ####
	scw = largura da coluna �nica
	m = margin (1.25)
	mc = m�ximo de colunas (4)
	
	cw = largura da coluna
	scw = largura da coluna �nica (24.0625%)
	cs = column span (1-4)
	m = margin (1.25%)
### Alterar colunas ###
Caso queria adicionar mais colunas basta refazer os c�lculos.

# Exemplo completo #

```html
<!--

	Sistemas de grid.
	@author Andr� Timm

-->
<!Doctype html>
<html lang="pt-br">
<head>
	<link rel="stylesheet" href="css/grid.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
</head>
<body>
	<div class="row">
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
	</div>

	<div class="row">
		<div class="col col-2">2</div>
		<div class="col col-1">1</div>
		<div class="col col-1">1</div>
	</div>

	<div class="row">
		<div class="col col-2">2</div>
		<div class="col col-2">2</div>
	</div>

	<div class="row">
		<div class="col col-3">3</div>
		<div class="col col-1">2</div>
	</div>

	<div class="row">
		<div class="col col-4">4</div>
	</div>
</body>
</html>
```